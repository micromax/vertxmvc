package com.sadeem.UltraVxCore;

import com.sadeem.UltraVxIface.iSessionStorge;

import java.util.ArrayList;

public class SessionStorge implements iSessionStorge {

    ArrayList<Session> Sessions = new ArrayList<>();

    @Override
    public Session getSessionById(int id) {

        return this.Sessions.get(id);

    }

    @Override
    public ArrayList<Session> getSessionStorge() {
        return this.Sessions;
    }

    @Override
    public boolean addSession(Session sessions) {
        return this.Sessions.add(sessions);
    }

    @Override
    public boolean deleteSessionByid(Session sessions) {
        return this.Sessions.remove(sessions);
    }


}
