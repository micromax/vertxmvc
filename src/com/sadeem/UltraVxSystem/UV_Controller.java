package com.sadeem.UltraVxSystem;

import com.sadeem.UltraVxCore.load;
import com.sadeem.UltraVxIface.iController;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class UV_Controller extends load implements iController  {

    public load load = new load();

    public RoutingContext getRxtx() {
        return rxtx;
    }

    public void setRxtx(RoutingContext rxtx) {
        this.rxtx = rxtx;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    private RoutingContext rxtx;
    private String URL ;


    public UV_Controller(RoutingContext rxtx){
        this.inil(rxtx);

    }


    @Override
    public void inil(RoutingContext rxtx)
    {
        this.rxtx = rxtx;
        this.URL = this.rxtx.request().absoluteURI();



    }

    @Override
    public void GlobalLoader(String sessionId) {

    }

    @Override
    public Map<String, String> GetExtraheader() {
        return null;
    }

    @Override
    public void setExtraheader(String Key, String Val) {

    }

    @Override
    public int GetExtraCode() {
        return 0;
    }

    @Override
    public void action() {

    }

    @Override
    public void prosses() {

    }

    @Override
    public String Result() {
        return null;
    }

    @Override
    public void index() throws IOException
    {

    }


    public void invoker(String MethodName) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException
    {




        Method method = this.getClass().getDeclaredMethod(MethodName);
        method.invoke(this);
    }



    public void Render(String s){

        HttpServerResponse response = this.getRxtx().response();

        response.end(s);
    }
    @Override
    public void finalize()
    {

    }
}
