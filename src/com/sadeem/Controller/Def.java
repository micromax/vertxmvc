package com.sadeem.Controller;

import com.sadeem.UltraVxSystem.UV_Controller;
import io.vertx.ext.web.RoutingContext;

public class Def extends UV_Controller {
    public Def(RoutingContext rxtx) {
        super(rxtx);
    }

    public void index(){

        Render(this.load.View("templates/default.ftl" , this.getRxtx()));
    }
}
