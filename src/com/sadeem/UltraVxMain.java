package com.sadeem;


import com.sadeem.UltraVxSystem.UV_Controller;
import com.sadeem.scala.RouterMVC;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;


public class UltraVxMain extends AbstractVerticle
{


    public java.util.Map<String, String> RouterMap = scala.collection.JavaConverters.mapAsJavaMapConverter(RouterMVC.Routers()).asJava();

    public static int port = 8183;
    public  UV_Controller GetController(String whichClass , RoutingContext Parm) {
        try {

                Class clazz = Class.forName(whichClass);
                // System.out.println(clazz.getName().toString());
                return (UV_Controller) clazz.getDeclaredConstructor(RoutingContext.class).newInstance(Parm);



        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void start() throws Exception {
        //SchemaGenerator.Runner.compile()

        Vertx vertx = this.vertx;
        HttpServer server = vertx.createHttpServer();
        //System.setProperty("vertx.disableFileCaching", "true");
        Router router = Router.router(vertx);

       // HttpServer server = vertx.createHttpServer();


        router.route("/static/*").handler(StaticHandler.create());


        router.route("/*").handler(routingContext -> {











            String URL = routingContext.request().path();
            String[] controller = URL.split("/");
            int URLSigmantSize = controller.length -1;



            String classz = "Defualt";
            String Method = null;
            UV_Controller builts ;
            switch (URLSigmantSize){

                case -1 :
                case 0 :
                    try {


                        builts = GetController(RouterMap.get(classz), routingContext) ;
                        builts.index();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                break;
                case 1 :


                    try {
                            classz = controller[1].toString();

                            builts = GetController(RouterMap.get(classz), routingContext) ;


                            builts.index();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2 :


                    try {
                        classz = controller[1].toString();
                        builts = GetController(RouterMap.get(classz), routingContext) ;
                        builts.invoker( controller[2].toString());


                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    break;
                default:

                    break;

            }

           System.out.println(URL);
            //System.out.println(controller.toString());



            //System.out.println(controller[1]);
            //System.out.println(controller[2]);
            //System.out.println(controller[3]);
            //System.out.println(URLSigmantSize);
            //System.out.println(routingContext.request().getParam("ox"));


            //HttpServerResponse response = routingContext.response();



            //response.end(URL);

        });


        server.requestHandler(router::accept).listen(port);

    }



    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx(new VertxOptions().setWorkerPoolSize(100));

        System.setProperty("vertx.disableFileCaching", "true");
        try {
            Verticle n = new UltraVxMain();
            vertx.deployVerticle(n);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }




}
